��    �      T  7  �      h  a  i     �     �     �     �     �          #  T   ;  �   �  �   d  c   �  �   `  �           }   -  �   �  '   �  �   �  �   g  �     Y   �  p   @  g   �  ^     Y   x  o   �  _   B   G   �      �   q  �   �  o"  �  R$  C   4&  �   x&  �   '  �   �'  �   C(    �(     �,  y   	-  �  �-  P   c0  �   �0  �   �1  :   s2  P   �2  9   �2  �   93  �   4  �   �4  {   b5  b   �5  ^   A6  R   �6  �   �6  g   �7  >  I8  �   �9  �  5:     �<  �   �<  I   �=  �   >  o   �>  w   1?  �   �?     c@  &   r@  #   �@  M   �@  =   A  :   IA  0   �A  /   �A  1   �A  @   B  U   XB     �B  Q   �B  P    C     qC  %   �C  %   �C     �C  3   �C  K   D     dD     vD  (   �D  (   �D     �D  0   �D  '   .E     VE     jE  <   E  *   �E  7   �E  	   F  ,   )F     VF     gF     uF  &   �F     �F  3   �F     �F  	   G     G  |   %G     �G     �G     �G  !   �G  #   H  m  >H     �I  #   �I  #   �I  (   J  9   <J  '   vJ     �J     �J  (   �J     �J     K     #K  %   ;K  :   aK  )   �K     �K  *   �K  )   �K     $L     7L     VL     gL     L     �L  Q   �L  A   �L  S   'M     {M     �M     �M     �M     �M  '   N     +N     BN     \N  4   tN     �N     �N     �N  %   �N  C   O  +   FO     rO     �O      �O  +   �O     �O  0   �O  !   .P  ,   PP     }P     �P     �P     �P  2   �P     Q     9Q     SQ  2   _Q     �Q     �Q      �Q  %   �Q  8   R     KR  .   cR      �R  1   �R     �R  -   �R     -S  1   :S     lS     �S     �S  "   �S  "   �S     �S  }   T  e   �T    �T  �   V     �V     �V     �V     �V     W     W  	   /W  =   9W  L   wW  F   �W  R   X  S   ^X  @   �X  '   �X  J   Y     fY     Y     �Y     �Y     �Y  !   �Y     �Y     �Y     Z     &Z     DZ     cZ     �Z     �Z     �Z  �  �Z    x\     �`     �`     �`     �`  .   �`  &   )a  &   Pa  �   wa  f  b  L  zc  �   �d  u  �e  �  +g     �h  �   i  �  �i  K   Yk  $  �k  %  �l  i  �m  �   Zo  �   	p  �   �p  �   rq  �   r  �   �r  �   �s  �   Ut     �t  \  u  1  lw  {  �z  ~   ~  �   �~  V  R  *  ��    ԁ  C  ݂  .   !�  �   P�  �  0�  �   ��  �  R�  n  ��  `   n�  �   ϔ  _   Y�  X  ��  x  �  �   ��  �   p�  �   V�  �   ��  �   Û  �  E�  �   &�  5  ۞    �  f  �     z�  j  ��  l   ��  &  k�  �   ��  �   \�  I  ;�      ��  Q   ��  R   ��  �   K�  ]   ҭ  N   0�  [   �  B   ۮ  W   �  ^   v�  d   կ  /   :�  d   j�  `   ϰ  %   0�  4   V�  ,   ��     ��  i   ʱ  �   4�  *   Ʋ  3   �  @   %�  ;   f�  &   ��  G   ɳ  4   �      F�  !   g�  ^   ��  U   �  r   >�     ��  H   µ     �  +   $�  %   P�  S   v�  &   ʶ  w   �  $   i�     ��     ��  �   ��  H   ��     �  G    �  P   H�  S   ��  �  ��  :   ��  4   ϼ  4   �  >   9�  b   x�  2   ۽  .   �  E   =�  X   ��  7   ܾ     �  %   2�  0   X�  l   ��  T   ��     K�  C   \�  B   ��  #   ��  9   �  #   A�  ;   e�     ��     ��  h   ��  {   #�  x   ��  '   �  L   @�  H   ��  ,   ��  8   �  `   <�  .   ��  1   ��  *   ��  O   )�     y�  $   ��  /   ��  ?   ��  �   (�  W   ��  -   �  <   E�  *   ��  F   ��      ��  S   �  6   i�  f   ��  /   �  /   7�  ,   g�  6   ��  T   ��  +    �  =   L�     ��  \   ��  5   �  5   7�  R   m�  :   ��  w   ��  5   s�  S   ��  4   ��  [   2�  1   ��  Q   ��     �  S    �      t�  =   ��  9   ��  I   �  U   W�  9   ��  �   ��  �   ��  '  ��    ��  !   ��  D   ��  .   /�  4   ^�  A   ��  5   ��     �  b   %�  q   ��  [   ��  �   V�  �   ��  w   m�  A   ��  c   '�     ��  3   ��  5   ��     �      �  7   6�  $   n�  #   ��  *   ��     ��     ��     ��     ��     ��     �     �   4   }       I   �   !          B       i              �           w   l           Z      \   �       �       �       J   �   �               j       _   g           )   �   L   �   �          �   �   �   �   7   o           <   �   �   b   �          �   �           "       U   �   �   +       �   ^           �   �   >   �   �   r   ;   �   s               8   @   H   �       x                   *   �       |   	   3       �   �      v   [           E   �   �   Y   2   �      �   c       �   X   �       M   �   �       n   �      �   �   '      ~   �   �   C   P      �   0   T   �   G   R           S          #   �   K       �           �   �   �          =   �   f   F       �       �   p   �       9       �   (   �          �   5              Q      �   &           �   �   �   O   �       �   �   �   :   d   �   %   �       ,   �       y      �       .   {   �   t   �   �   q   �   �   �       �   A   �   1   k   
       �   �       m   �          �   e      ?   /   �       �       -   z   �   �          $   �         �              D   �   �   N   W       �   �   �      �   �                 a          h   �   �         �       ]       �   �   �      `   6               �   V   u   �    
    Start the named runner, which must be one of the strings returned by the -l
    option.

    For runners that manage a queue directory, optional `slice:range` if given
    is used to assign multiple runner processes to that queue.  range is the
    total number of runners for the queue while slice is the number of this
    runner from [0..range).  For runners that do not manage a queue, slice and
    range are ignored.

    When using the `slice:range` form, you must ensure that each runner for the
    queue is given the same range value.  If `slice:runner` is not given, then
    1:1 is used.
     
- Done. 
- Ignored: 
- Results: 
- Unprocessed: 
Held Messages:
 
Held Subscriptions:
 
Held Unsubscriptions:
     A more verbose output including the file system paths that Mailman is
    using.     Add all member addresses in FILENAME with delivery mode as specified
    with -d/--delivery.  FILENAME can be '-' to indicate standard input.
    Blank lines and lines that start with a '#' are ignored.
         Additional metadata key/value pairs to add to the message metadata
    dictionary.  Use the format key=value.  Multiple -m options are
    allowed.     Change a user's email address from old_address to possibly case-preserved
    new_address.
         Configuration file to use.  If not given, the environment variable
    MAILMAN_CONFIG_FILE is consulted and used if set.  If neither are given, a
    default configuration file is loaded.     Create a mailing list.

    The 'fully qualified list name', i.e. the posting address of the mailing
    list is required.  It must be a valid email address and the domain must be
    registered with Mailman.  List names are forced to lower case.     Date: ${date}     Delete all the members of the list.  If specified, none of -f/--file,
    -m/--member or --fromall may be specified.
         Delete list members whose addresses are in FILENAME in addition to those
    specified with -m/--member if any.  FILENAME can be '-' to indicate
    standard input.  Blank lines and lines that start with a '#' are ignored.
         Delete members from a mailing list.     Delete the list member whose address is ADDRESS in addition to those
    specified with -f/--file if any.  This option may be repeated for
    multiple addresses.
         Delete the member(s) specified by -m/--member and/or -f/--file from all
    lists in the installation.  This may not be specified together with
    -a/--all or -l/--list.
         Display a mailing list's members.
    Filtering along various criteria can be done when displaying.
    With no options given, displaying mailing list members
    to stdout is the default mode.
         Display all memberships for a user or users with address matching a
    pattern.
         Display only memberships with the given role.  If not given, 'all' role,
    i.e. all roles, is the default.     Display output to FILENAME instead of stdout.  FILENAME
    can be '-' to indicate standard output.     Don't actually do anything, but in conjunction with --verbose, show what
    would happen.     Don't print status messages.  Error messages are still printed to standard
    error.     Don't restart the runners when they exit because of an error or a SIGUSR1.
    Use this only for debugging.     File to send the output to.  If not given, or if '-' is given, standard
    output is used.     File to send the output to.  If not given, standard output is used.     From: ${from_}     Generate the MTA alias files upon startup. Some MTA, like postfix, can't
    deliver email if alias files mentioned in its configuration are not
    present. In some situations, this could lead to a deadlock at the first
    start of mailman3 server. Setting this option to true will make this
    script create the files and thus allow the MTA to operate smoothly.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option, the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If the master watcher finds an existing master lock, it will normally exit
    with an error message.  With this option,the master will perform an extra
    level of checking.  If a process matching the host/pid described in the
    lock file is running, the master will still exit, requiring you to manually
    clean up the lock.  But if no matching process is found, the master will
    remove the apparently stale lock and make another attempt to claim the
    master lock.     If you are sure you want to run as root, specify --run-as-root.     Import Mailman 2.1 list data.  Requires the fully-qualified name of the
    list to import and the path to the Mailman 2.1 pickle file.     Increment the digest volume number and reset the digest number to one.  If
    given with --send, the volume number is incremented before any current
    digests are sent.     Key to use for the lookup.  If no section is given, all the key-values pair
    from any section matching the given key will be displayed.     List only those mailing lists hosted on the given domain, which
    must be the email host name.  Multiple -d options may be given.
         Master subprocess watcher.

    Start and watch the configured runners, ensuring that they stay alive and
    kicking.  Each runner is forked and exec'd in turn, with the master waiting
    on their process ids.  When it detects a child runner has exited, it may
    restart it.

    The runners respond to SIGINT, SIGTERM, SIGUSR1 and SIGHUP.  SIGINT,
    SIGTERM and SIGUSR1 all cause a runner to exit cleanly.  The master will
    restart runners that have exited due to a SIGUSR1 or some kind of other
    exit condition (say because of an uncaught exception).  SIGHUP causes the
    master and the runners to close their log files, and reopen then upon the
    next printed message.

    The master also responds to SIGINT, SIGTERM, SIGUSR1 and SIGHUP, which it
    simply passes on to the runners.  Note that the master will close and
    reopen its own log files on receipt of a SIGHUP.  The master also leaves
    its own process id in the file specified in the configuration file but you
    normally don't need to use this PID directly.     Message-ID: ${message_id}     Name of file containing the message to inject.  If not given, or
    '-' (without the quotes) standard input is used.     Normally, this script will refuse to run if the user id and group id are
    not set to the 'mailman' user and group (as defined when you configured
    Mailman).  If run as root, this script will change to this user and group
    before the check is made.

    This can be inconvenient for testing and debugging purposes, so the -u flag
    means that the step that sets and checks the uid/gid is skipped, and the
    program is run as the current user and group.  This flag is not recommended
    for normal production environments.

    Note though, that if you run with -u and are not in the mailman group, you
    may have permission problems, such as being unable to delete a list's
    archives through the web.  Tough luck!     Notify the list owner by email that their mailing list has been
    created.     Operate on this mailing list.  Multiple --list options can be given.  The
    argument can either be a List-ID or a fully qualified list name.  Without
    this option, operate on the digests for all mailing lists.     Override the default set of runners that the master will invoke, which is
    typically defined in the configuration file.  Multiple -r options may be
    given.  The values for -r are passed straight through to bin/runner.     Override the list's setting for admin_notify_mchanges.     Override the list's setting for send_goodbye_message to
    deleted members.     Override the list's setting for send_welcome_message.     Register the mailing list's domain if not yet registered.  This is
    the default behavior, but these options are provided for backward
    compatibility.  With -D do not register the mailing list's domain.     Run the named runner exactly once through its main loop.  Otherwise, the
    runner runs indefinitely until the process receives a signal.  This is not
    compatible with runners that cannot be run once.     Running mailman commands as root is not recommended and mailman will
    refuse to run as root unless this option is specified.     Section to use for the lookup.  If no key is given, all the key-value pairs
    of the given section will be displayed.     Send any collected digests for the List only if their digest_send_periodic
    is set to True.     Send any collected digests right now, even if the size threshold has not
    yet been met.     Send the added members an invitation rather than immediately adding them.
         Set the added members delivery mode to 'regular', 'mime', 'plain',
    'summary' or 'disabled'.  I.e., one of regular, three modes of digest
    or no delivery.  If not given, the default is regular.  Ignored for invited
    members.     Set the list's preferred language to CODE, which must be a registered two
    letter language code.     Specify a list owner email address.  If the address is not currently
    registered with Mailman, the address is registered and linked to a user.
    Mailman will send a confirmation message to the address, but it will also
    send a list creation notice to the address.  More than one owner can be
    specified.     Specify the encoding of strings in PICKLE_FILE if not utf-8 or a subset
    thereof. This will normally be the Mailman 2.1 charset of the list's
    preferred_language.     Start a runner.

    The runner named on the command line is started, and it can either run
    through its main loop once (for those runners that support this) or
    continuously.  The latter is how the master runner starts all its
    subprocesses.

    -r is required unless -l or -h is given, and its argument must be one of
    the names displayed by the -l switch.

    Normally, this script should be started from `mailman start`.  Running it
    separately or with -o is generally useful only for debugging.  When run
    this way, the environment variable $MAILMAN_UNDER_MASTER_CONTROL will be
    set which subtly changes some error handling behavior.
         Subject: ${subject}     The gatenews command is run periodically by the nntp runner.
    If you are running it via cron, you should remove it from the crontab.
    If you want to run it manually, set _MAILMAN_GATENEWS_NNTP in the
    environment.     The list to operate on.  Required unless --fromall is specified.
         The name of the queue to inject the message to.  QUEUE must be one of the
    directories inside the queue directory.  If omitted, the incoming queue is
    used.     [MODE] Add all member addresses in FILENAME.  This option is removed.
    Use 'mailman addmembers' instead.     [MODE] Delete all member addresses found in FILENAME.
    This option is removed. Use 'mailman delmembers' instead.     [MODE] Synchronize all member addresses of the specified mailing list
    with the member addresses found in FILENAME.
    This option is removed. Use 'mailman syncmembers' instead.  (Digest mode) ${count} matching mailing lists found: ${display_name} post acknowledgment ${member} unsubscribed from ${mlist.display_name} mailing list due to bounces ${member}'s bounce score incremented on ${mlist.display_name} ${member}'s subscription disabled on ${mlist.display_name} ${mlist.display_name} mailing list probe message ${mlist.display_name} subscription notification ${mlist.display_name} unsubscription notification ${mlist.fqdn_listname} post from ${msg.sender} requires approval ${mlist.list_id} bumped to volume ${mlist.volume}, number ${mlist.next_digest_number} ${mlist.list_id} has no members ${mlist.list_id} is at volume ${mlist.volume}, number ${mlist.next_digest_number} ${mlist.list_id} sent volume ${mlist.volume}, number ${mlist.next_digest_number} ${name} runs ${classname} ${person} left ${mlist.fqdn_listname} ${realname} via ${mlist.display_name} (no subject) --send and --periodic flags cannot be used together A previous run of GNU Mailman did not exit cleanly ({}).  Try using --force Accept a message. Address changed from {} to {}. Address {} already exists; can't change. Address {} is not a valid email address. Address {} not found. Addresses are not different.  Nothing to change. Already subscribed (skipping): ${email} An alias for 'end'. An alias for 'join'. An alternative directory to output the various MTA files to. Cannot import runner module: ${class_path} Cannot parse as valid email address (skipping): ${line} Confirmed Created mailing list: ${mlist.fqdn_listname} DMARC moderation Digest Footer Digest Header Discard a message and stop processing. Display Mailman's version. Display more debugging information to the log file. Echo back your arguments. Email: {} End of  For unknown reasons, the master lock could not be acquired.

Lock file: ${config.LOCK_FILE}
Lock host: ${hostname}

Exiting. Forward of moderated message GNU Mailman is already running Generating MTA alias maps Header "{}" matched a header rule Hold a message and stop processing. If you reply to this message, keeping the Subject: header intact, Mailman will
discard the held message.  Do this if the message is spam.  If you reply to
this message and include an Approved: header with the list password in it, the
message will be approved for posting to the list.  The Approved: header can
also appear in the first line of the body of the reply. Ignoring non-dictionary: {0!r} Illegal list name: ${fqdn_listname} Illegal owner addresses: ${invalid} Information about this Mailman instance. Inject a message from a file into a mailing list's queue. Invalid language code: ${language_code} Is the master even running? Join this mailing list. Last autoresponse notification for today Leave this mailing list. Less verbosity List all mailing lists. List already exists: ${fqdn_listname} List only those mailing lists that are publicly advertised List the available runner names and exit. List: {} Member not subscribed (skipping): ${email} Membership is banned (skipping): ${email} Message ${message} Message contains administrivia Moderation chain Modify message headers. N/A Name: ${fname}
 New subscription request to ${self.mlist.display_name} from ${self.address.email} New unsubscription request from ${mlist.display_name} by ${email} New unsubscription request to ${self.mlist.display_name} from ${self.address.email} No child with pid: ${pid} No confirmation token found No matching mailing lists found No runner name given. No such list found: ${spec} No such list matching spec: ${listspec} No such list: ${_list} No such list: ${listspec} No such queue: ${queue} Not a Mailman 2.1 configuration file: ${pickle_file} Nothing to do Operate on digests. Original Message PID unreadable in: ${config.PID_FILE} Poll the NNTP server for messages to be gatewayed to mailing lists. Posting of your message titled "${subject}" Print less output. Print some additional status. Print the Mailman configuration. Process DMARC reject or discard mitigations Reason: {}

 Regenerate the aliases appropriate for your MTA. Regular expression requires --run Reject/bounce a message and stop processing. Remove DomainKeys headers. Remove a mailing list. Removed list: ${listspec} Reopening the Mailman runners Request to mailing list "${display_name}" rejected Restarting the Mailman runners Send automatic responses. Sender: {}
 Show a list of all available queue names and exit. Show also the list descriptions Show also the list names Show this help message and exit. Shutting down Mailman's master runner Signal the Mailman processes to re-open their log files. Stale pid file removed. Start the Mailman master and runner processes. Starting Mailman's master runner Stop and restart the Mailman runner subprocesses. Stop processing commands. Stop the Mailman master and runner processes. Subject: {}
 Subscription already pending (skipping): ${email} Subscription request Suppress status messages The Mailman Replybot The built-in -owner posting chain. The built-in header matching chain The built-in moderation chain. The content of this message was lost. It was probably cross-posted to
multiple lists and previously handled on another list.
 The master lock could not be acquired because it appears as though another
master is already running. The master lock could not be acquired, because it appears as if some process
on some other host may have acquired it.  We can't test for stale locks across
host boundaries, so you'll have to clean this up manually.

Lock file: ${config.LOCK_FILE}
Lock host: ${hostname}

Exiting. The master lock could not be acquired.  It appears as though there is a stale
master lock.  Try re-running ${program} with the --force flag. Today's Topics:
 Uncaught bounce notification Undefined domain: ${domain} Undefined runner name: ${name} Unshunt messages. Unsubscription request User: {}
 Welcome to the "${mlist.display_name}" mailing list${digmode} You have been invited to join the ${event.mlist.fqdn_listname} mailing list. You have been unsubscribed from the ${mlist.display_name} mailing list Your confirmation is needed to join the ${event.mlist.fqdn_listname} mailing list. Your confirmation is needed to leave the ${event.mlist.fqdn_listname} mailing list. Your message to ${mlist.fqdn_listname} awaits moderator approval Your new mailing list: ${fqdn_listname} Your subscription for ${mlist.display_name} mailing list has been disabled [${mlist.display_name}]  [----- end pickle -----] [----- start pickle -----] [ADD] %s [DEL] %s [No bounce details are available] [No reason given] [No reasons given] bad argument: ${argument} list:member:digest:header.txt list:member:regular:header.txt list:user:notice:goodbye.txt n/a not available {} Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2022-09-13 09:16+0000
Last-Translator: Sergii Horichenko <m@sgg.im>
Language-Team: Ukrainian <https://hosted.weblate.org/projects/gnu-mailman/mailman/uk/>
Language: uk
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Weblate 4.14.1-dev
 
    Запустіть названий потік, який має бути одним із рядків, які повертає
    параметр -l.

    Для потоків, які керують каталогом черги, якщо вказано необов'язковий "slice:range",
    використовується для призначення кількох процесів потоку цій черзі. range є
    загальною кількістю потоків для черги, а slice - це кількість цього
    потоку з [0..діапазон). Для потоків, які не керують чергою, slice та
    range ігноруються.

    Використовуючи форму "slice:range", Ви повинні переконатися, що кожному потоку для
    черги надається те саме значення діапазону (range). Якщо "slice:runner" не вказано,
    використовується 1:1.
     
- Виконано. 
- Проігноровано: 
- Результати: 
- Необроблене: 
Затримані повідомлення:
 
Затримані підписки:
 
Затримані відписки:
     Більш детально із зазначенням шляхів до файлової системи,
    які використовує Mailman.     Додайте адреси всіх учасників у FILENAME із указаним режимом доставки
    з -d/--delivery.  FILENAME може бути '-' для позначення стандартного вводу.
    Порожні рядки та рядки, які починаються з "#", ігноруються.
         Додаткові пари метаданих ключ/значення для додавання до метаданих словника
    повідомлення.  Використовуйте формат ключ=значення.  Допускається використання
    декількох опцій -m.     Змініть адресу електронної пошти користувача зі старої адреси на нову з можливим регістром
    великими/маленькими літерами.
         Файл конфігурації для використання.  Якщо не вказано, використовується
    змінна середовища MAILMAN_CONFIG_FILE, якщо вона встановлена. Якщо нічого
    з цього не задано, завантажується типовий файл конфігурації.     Створіть список розсилки.

    Необхідно вказати "повне ім'я списку", тобто адресу ел.пошти списку
    розсилки.  Це має бути дійсна адреса ел.пошти та домен повинен бути
    зареєстрованим у Mailman.  Назви списків примусово вводяться в нижньому регістрі.     Дата: ${date}     Видаляє усіх учасників списку. Якщо вказано, жоден із -f/--file,
    -m/--member або --fromall не використовується.
         Видаляє учасників списку, адреси яких містяться у FILENAME, або вказані
    з параметром -m/--member, якщо такий є.  FILENAME може бути "-" для позначення
    стандартного входу.  Порожні рядки та рядки, які починаються з "#", ігноруються.
         Видалити учасників зі списку розсилки.     Видаляє учасника списку, адреса якого є АДРЕСОЮ, а також вказані
    з параметром -f/--file, якщо такий є.  Ця опція може використовуватися
    для декількох адрес.
         Видалити учасників, визначених параметрами -m/--member та/або -f/--file
    з усіх списків розсилки.  Це не можна використовувати разом з параметрами
    -a/--all або -l/--list.
         Показати учасників списку розсилки.
    Можна зробити фільтрацію за різними критеріями.
    Якщо ніяких параметрів не задано, то відображення учасників списку розсилки
    в stdout є типовим режимом.
         Відобразити всі членства користувача або користувачів, адреса яких відповідає
    шаблону.
         Відображення членства лише з указаною роллю.  Якщо не задано, то типове 
    значення ролі "all", тобто усі ролі.     Виведення у FILENAME замість stdout.  FILENAME
    може бути '-' для позначення стандартного виводу.     Насправді нічого не робіть, але в поєднанні з --verbose, показує, що би
    сталося.     Не друкувати повідомлення про стан.  Повідомлення про помилки друкуються
    разом зі стандартними помилками.     Не перезапускайте потоки, коли вони виходять через помилку або SIGUSR1.
    Використовуйте це лише для налагодження.     Файл, куди направити запис. Якщо не вказано або вказано "-", використовується
    стандартний вихід.     Файл, у який буде надіслано вивід.  Якщо не вказано, використовується стандартний вивід.     Від: ${from_}     Створити alias-файли MTA під час запуску. Деякі MTA, такі як Postfix, не
    можуть доставляти ел.пошту, якщо alias-файли, згадані в його конфігурації,
    відсутні. У деяких ситуаціях це може призвести до зупинки на першому
    запуску сервера mailman3. Встановлюючи цей параметр у значення
    true, цей скрипт створить файли і надасть змогу MTA працювати.     Коли Master Watcher знаходить існуючий майстер-лок, він зазвичай виходить
    з помилкою.  З даним параметром, майстер буде робити перевірку додаткового
    рівня.  Якщо процес відповідає хосту/PID, зазначеному у файлі блокування
    (лок-файлі), майстер також вийде, але з необхідністю Вами очищення цього
    блокування вручну.  Але якщо відповідний процес не буде знайдений,
    майстер видалить застарілий лок та зробить іншу спробу створення
    майстер-локу.     Якщо головний спостерігач знаходить наявне головне блокування, він, як правило,
    завершується з повідомленням про помилку.  За допомогою цієї опції майстер виконає
    додаткову перевірку.  Якщо процес відповідає хосту/pid, описаному у файлі блокування,
    майстер все одно завершиться, потребуючи від Вас ручного очищення
    блокування.  Але якщо відповідний процес не буде знайдений, майстер
    вилучить застаріле блокування і зробить ще одну спробу отримати
    ексклюзивне блокування.     Якщо Ви впевнені, що хочете запускати від імені root, вкажіть --run-as-root.     Імпорт даних списку Mailman 2.1.  Потрібна повна назва імпортованого
    списку та шлях до pck-файлу Mailman 2.1.     Збільшує номер тому дайджеста та скидає номер дайджеста до одиниці.  Якщо
    використовується параметр --send, номер тому збільшується перед надсиланням
    будь-якого поточного дайджеста.     Ключ для використання під час пошуку.  Якщо розділ не заданий, будуть відображені усі пари ключ-значення
    з будь-якого розділу, які відповідають заданому ключу.     Список лише тих списків розсилки, які розміщені на даному домені, який
    має бути ім'ям хоста ел.пошти.  Можна вказати декілька параметрів -d.
         Головний спостерігач за підпроцесами.

    Почніть і спостерігайте за налаштованими потоками, щоб вони залишилися робочими та
    діючими.  Кожен потік роздвоєний і виконується по черзі, а майстер чекає
    на їх ID процесів.  Коли він виявляє, що наслідковий потік завершився, він може
    перезапустити його.

    Потоки реагують на SIGINT, SIGTERM, SIGUSR1 та SIGHUP.  SIGINT,
    SIGTERM та SIGUSR1 примушують потік виходити чисто.  Майстер буде
    перезапускати процеси, які завершилися через SIGUSR1 або за будь-якої іншої
    умови виходу (наприклад, через незловлений виняток).  SIGHUP примушує
    майстер і потоки закривати свої файли журналів, а потім знову відкривати після
    наступного друкованого повідомлення.

    Майстер також відповідає на SIGINT, SIGTERM, SIGUSR1 та SIGHUP, які він
    просто передає потокам.  Прийміть до уваги, що майстер закриє та
    відкриє заново власні файли журналів після отримання SIGHUP.  Майстер також
    залишає власний ID процесу у файлі, вказаному у файлі конфігурації, але Вам
    зазвичай не потрібно використовувати цей PID безпосередньо.     ID повідомлення: ${message_id}     Ім'я файлу, що містить повідомлення для вставки.  Якщо не вказано, або
    '-' (без лапок) використовується стандартний вхід.     Зазвичай, цей скрипт відмовлятиме у запуску, якщо ідентифікатори
    користувача та групи не встановлені відповідно "mailman" та назва групи,
    визначена під час налаштування mailman.  Якщо запустити цей скрипт від імені
    root, він замінить на цього користувача і на цю групу перед виконанням перевірки.

    Це може бути незручним для тестування і налагодження, тому ключ -u означає,
    що крок, який встановлює і перевіряє uid/gid пропускається і програма
    запускається від поточного користувача і групи.  Цей ключ не рекомендується
    для звичайного робочого середовища.

    Однак, зверніть увагу, що якщо Ви запускаєте з параметром -u і не входите до групи
    mailman, у Вас можуть виникнути проблеми з дозволами, на кшалт неможливості
    видалення архівів списку через мережу. Тож тримайтеся!     Повідомити власника списку ел.поштою про те, що його список розсилки
    створений.     Обробка цього списку розсилки.  Можна задати кілька параметрів --list.  Також
    аргументом може бути ідентифікатор списку (List-ID) або повна назва списку.  Без
    цього параметру, обробка дайджестів здійснюється для всіх списків розсилки.     Перевизначення типового набору потоків, яке буде викликати майстер, яке
    зазвичай визначається у файлі конфігурації. Може бути кілька параметрів -r.
    Значення для -r передаються прямо до bin/потоку.     Перевизначає налаштування списку для admin_notify_mchanges.     Перевизначає налаштування списку send_goodbye_message для
    видалених учасників.     Перевизначте налаштування списку для send_welcome_message.     Зареєструвати домен розсилки, якщо він ще не зареєстрований.  Це типова
    поведінка, але ці опції надані для зворотної
    сумісності.  За допомогою -D не реєструвати домен списку розсилки.     Запустити названий потік рівно один раз через його головний цикл. В іншому випадку,
    потік працює нескінченно, поки процес не отримає сигнал. Це не
    сумісно з потоками, які неможливо запустити один раз.     Виконувати команди mailman від імені root не бажано, отже mailman не буде
    працювати від імені root, доки ця опція не буде активована.     Розділ для використання під час пошуку.  Якщо ключ не задано, будуть відображені усі пари
    ключ-значення заданого розділу.     Надсилає будь-які зібрані дайджести до Списку лише якщо їх digest_send_periodic
    має значення True.     Надсилає будь-які зібрані дайджести прямо зараз, навіть якщо не досягнутий
    необхідне значення розміру.     Надішліть доданим учасникам запрошення, а не одразу додавайте їх.
         Встановіть режим доставки для доданих учасників на "звичайний", "mime", "простий",
    "підсумки" або "вимкнено".  Тобто, один зі звичайних трьох режимів дайджесту
    або без доставки.  Якщо не задано, типове значення "звичайний".  Ігнорується
    для запрошених учасників.     Встановіть бажану мову списку на CODE, який повинен бути зареєстрованим
    дволітерним кодом мови.     Вкажіть адресу ел.пошти власника списку.  Якщо адреса наразі
    не зареєстрована в Mailman, адреса буде зареєстрована і прив'язана до користувача.
    Mailman надішле на цю адресу повідомлення для підтвердження, а також
    надішле сповіщення про створення списку на цю адресу.  Можна вказати більше
    одного власника.     Вказажіть кодування рядків у PICKLE_FILE, якщо це не utf-8 або не його підмножина.
    Зазвичай це буде кодування Mailman 2.1 preferred_language
    для мови списку.     Запуск потоку.

    Запущено потік, названий у командному рядку, і він може запускатися
    через його основний цикл один раз (для тих потоків, які це підтримують) або
    постійно.  Останнє – це те, як майстер-потік запускає усі свої
    підпроцеси.

    -r є обов'язковим, якщо не задано -l або -h, і його аргумент повинен бути одним з
    імен, які відображаються за перемикачем -l.

    Зазвичай, цей скрипт повинен починатися з "mailman start".  Запуск
    окремо або з -o взагалі корисний тільки для налагодження.  Якщо запускати
    таким чином, буде встановлена змінна середовища $MAILMAN_UNDER_MASTER_CONTROL,
    яка тонко змінює обробку деяких помилок.
         Тема: ${subject}     Команда gatenews періодично виконується потоком nntp.
    Якщо Ви запускаєте її через cron, Вам потрібно видалити її з crontab.
    Якщо Ви хочете запускати її вручну, установіть в оточенні змінну
    _MAILMAN_GATENEWS_NNTP.     Список для обробки.  Обов'язково, якщо не вказано --fromall.
         Ім'я черги, в яку потрібно вставити повідомлення.  QUEUE має бути одним з каталогів
    всередині каталогу черги.  Якщо не вказано, використовується вхідна
    черга.     [MODE] Додати всі адреси учасників у FILENAME.  Цей параметр вилучений.
    Замість цього використовуйте 'mailman addmembers'.     [MODE] Видалити всі адреси учасників, знайдені у FILENAME.
    Цей параметр вилучений. Замість цього використовуйте 'mailman delmembers'.     [MODE] Синхронізувати всі адреси учасників вказаного списку розсилки
    з адресами учасників, знайденими в FILENAME.
    Цей параметр вилучено. Замість цього використовуйте "mailman syncmembers".  (Режим дайджесту) Знайдено ${count} відповідних списків розсилки: ${display_name} підтверджує отримання повідомлення ${member} відписаний від списку розсилки ${mlist.display_name} через повернення листів Лічильник повернень ${member} збільшений з ${mlist.display_name} Підписка ${member} на ${mlist.display_name} деактивована тестове повідомлення списку розсилки ${mlist.display_name} Сповіщення про підписку ${mlist.display_name} Сповіщення про скасування підписки ${mlist.display_name} Допису ${mlist.fqdn_listname} від ${msg.sender} необхідне схвалення ${mlist.list_id} перенесено до тому ${mlist.volume}, № ${mlist.next_digest_number} ${mlist.list_id} не має учасників ${mlist.list_id} знаходиться у томі ${mlist.volume}, № ${mlist.next_digest_number} ${mlist.list_id} надіслано у томі ${mlist.volume}, № ${mlist.next_digest_number} ${name} запускає ${classname} ${person} виходить з ${mlist.fqdn_listname} ${realname} через ${mlist.display_name} (без теми) Параметри --send та --periodic не можуть разом використовуватися Попередній запуск GNU Mailman завершився неправильно ({}). Спробуйте використати --force Прийняти повідомлення. Адреса була змінена з {} на {}. Адреса {} вже існує; змінити не можу. Адреса {} не є дійсною ел.адресою. Адреса {} не знайдена. Адреси однакові.  Немає чого змінювати. Вже підписано (ігнорую): ${email} Псевдоним для "end". Псевдоним для "join". Альтернативний каталог для запису різних файлів MTA. Не вдається імпортувати модуль потоку: ${class_path} Не вдається розібрати правильну адресу ел.пошти (ігнорую): ${line} Ухвалено Створений список розсилки: ${mlist.fqdn_listname} Модерація DMARC Футер(підпис) Дайджеста Заголовок Дайджеста Відкинути повідомлення та припинити обробку. Показати версію Mailman. Відображати більше налагоджувальної інформації у файлі журналу. Повторює аргументи. Ел.пошта: {} Закінчення  З невідомих причин загальне блокування зробити не вдалося.

Файл блокування: ${config.LOCK_FILE}
Хост блокування: ${hostname}

Виходимо. Пересилання модерованого повідомлення GNU Mailman вже працює Створення карт псевдонімів (aliases) для MTA Заголовок "{}" відповідає правилам заголовка Затримати повідомлення та припинити обробку. Якщо Ви відповісте на це повідомлення, залишивши заголовок Subject: недоторканим, Mailman
відкине утримування повідомлення.  Робіть так, якщо повідомлення є спамом.  Якщо Ви відповісте на
це повідомлення та додасте заголовок Approved: із паролем списку в ньому,
повідомлення буде схвалено для розміщення в списку.  Заголовок Approved: може
також з'явитися в першому рядку тіла відповіді. Ігнорування несловникового: {0!r} Хибна назва списку: ${fqdn_listname} Хибна адреса власника: ${invalid} Інформація про цей екземпляр Mailman. Вставте повідомлення з файлу в чергу списку розсилки. Хибний мовний код: ${language_code} Майстер-потік вже працює? Приєднатися до цього списку розсилки. Останнє на сьогодні сповіщення автовідповідача Вийти з цього списку розсилки. Менше подробиць Показати усі списки. Список вже існує: ${fqdn_listname} Спиок лише тих списків розсилки, які публічно рекламуються Перерахувати доступні імена потоків та вийти. Список: {} Учасник не підписаний (ігнорую): ${email} Членство заблоковане (ігнорую): ${email} Повідомлення ${message} Повідомлення містить адмін-дії Ланцюжок модерації Змінити заголовки повідомлення. Н/Д Ім'я: ${fname}
 Новий запит на підписку на ${self.mlist.display_name} від ${self.address.email} Новий запит на скасування підписки від ${mlist.display_name} з ел.поштою ${email} Новий запит на скасування підписки ${self.mlist.display_name} від ${self.address.email} Нащадку з pid ${pid} немає Жодного токена підтвердження не знайдено Відповідні списки розсилки не знайдені Назва потоку не вказана. Такий список не знайдений: ${spec} Немає списку який би відповідав специфікації: ${listspec} Такого списку немає: ${_list} Такого списку немає: ${listspec} Такої черги немає: ${queue} Не є конфігураційним файлом Mailman 2.1: ${pickle_file} Завдань немає Обробка дайджестів. Оригінальне повідомлення Прочитати PID з ${config.PID_FILE} неможливо Запитує сервер NNTP щодо повідомлень, які повинні передаватись до списків розсилки. Публікація Вашого повідомлення з назвою "${subject}" Показувати менше виводу. Показує деякі додаткові статуси. Друк конфігурації Mailman. Процес DMARC відхиляє або відкидає обхід Обґрунтування: {}

 Згенеруйте нові псевдоними (aliases) для свого MTA. Регулярний вираз потребує --run Відхилити/повернути повідомлення та припинити обробку. Вилучити заголовки DomainKeys. Вилучити список розсилки. Вилучений список: ${listspec} Повторний запуск потоків Mailman Запит до списку розсилки "${display_name}" відхилений Перезапуск потоків Mailman Надіслати автоматичну відповідь. Відправник: {}
 Показати список усіх доступних імен черг та вийти. Показати також описи списків Показати також назви списків Показати це довідкове повідомлення та вийти. Вимкнення головного потоку Mailman Сигналізувати до процесів Mailman перевідкрити свої файли журналів. Застарілий файл pid видалений. Запустіть головний та додаткові процеси Mailman. Запуск головного потоку Mailman Зупинити та перезапустити підпроцеси потоку Mailman. Зупинити виконання команд. Зупиніть основний та додаткові процеси Mailman. Тема: {}
 Підписка вже очікує на розгляд (ігнорую): ${email} Запит на підписку Заборонити повідомлення про стан Бот Mailman Replybot для автовідповіді Вбудований ланцюжок публікації для -owner. Вбудований ланцюжок відповідності заголовків Вбудований ланцюжок модерації. Зміст цього повідомлення втрачений. Можливо, воно було надіслане
у декілька списків і вже раніше було оброблене в іншому списку.
 Одноосібне використання в якості майстра не можна отримати, оскільки
схоже, що інший майстер вже активний. Одноосібне блокування в якості майстра не можна зробити, оскільки схоже, що якийсь
процес на якомусь іншому хості, вже отримав його. Ми не можемо перевірити наявність старих
блокувань за межами хосту, тож Вам доведеться вирішити це вручну.

Файл блокування: ${config.LOCK_FILE}
Хост блокування: ${hostname}

Виходимо. Одноосібне використання в якості майстра не можна отримати. Схоже існує застаріле блокування.
Спробуйте перезапустити ${program} з параметром --force. Сьогоднішні Теми:
 Сповіщення про поверення не отримане Невизначений домен: ${domain} Невизначене ім'я потоку: ${name} Відкинути відкладені повідомлення. Запит на скасування підписки Користувач: {}
 Ласкаво просимо до списку розсилки "${mlist.display_name}"${digmode} Вас запросили приєднатися до списку розсилки ${event.mlist.fqdn_listname}. Ви були відписані від списку розсилки ${mlist.display_name} Щоб приєднатися до списку розсилки ${event.mlist.fqdn_listname}, необхідне Ваше погодження. Щоб залишити список розсилки ${event.mlist.fqdn_listname}, необхідне Ваше погодження. Ваше повідомлення до ${mlist.fqdn_listname} очікує на схвалення модератора Ваш новий список розсилки: ${fqdn_listname} Ваша підписка на список розсилки ${mlist.display_name} вимкнена [${mlist.display_name}]  [----- кінець серіалізації -----] [----- початок серіалізації -----] [ДОДАТИ] %s [ВИДАЛИТИ] %s [Немає деталей про повернення] [Причина не вказана] [Без обґрунтування] хибний параметр: ${argument}        н.д. не доступне {} 